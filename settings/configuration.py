import os
from datetime import timedelta

from dotenv import load_dotenv, find_dotenv
from pymongo.read_preferences import ReadPreference

# Load environment
load_dotenv(find_dotenv())

# get absolute path static directory in root project
log_folder = os.path.abspath(os.path.join(
    os.path.dirname(os.path.dirname(__file__)), 'log'))
jwt_algorithm = os.getenv("JWT_ALGORITHM")
jwt_secret_key = os.getenv("JWT_SECRET_KEY")

postgre_host = os.getenv("POSTGRE_HOST")
postgre_port = os.getenv("POSTGRE_PORT")
postgre_db = os.getenv("POSTGRE_DB")
postgre_user = os.getenv("POSTGRE_USER")
postgre_password = os.getenv("POSTGRE_PASSWORD")

mongo_host = os.getenv("MONGO_HOST")
mongo_port = os.getenv("MONGO_PORT")
mongo_db = os.getenv("MONGO_DB")
mongo_user = os.getenv("MONGO_USER")
mongo_password = os.getenv("MONGO_PASSWORD")


class Configuration(object):
    # Basic
    DEBUG = os.getenv("DEBUG") == "True"
    PORT = int(os.getenv("PORT", 5000))

    # JWT
    JWT_ALGORITHM = os.getenv("JWT_ALGORITHM")
    JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY")
    JWT_AUTH_URL_RULE = None
    JWT_EXPIRATION_DELTA = timedelta(days=7)  # token expired in 1 weeks

    # MONGO
    MONGO_URI = "mongodb://"+mongo_user+":" + \
        mongo_password+"@"+mongo_host+":"+mongo_port+"/"+mongo_db
    MONGO_READ_PREFERENCE = ReadPreference.SECONDARY_PREFERRED

    # POSTGRESQL
    SQLALCHEMY_DATABASE_URI = "postgresql://"+postgre_user+":" + \
        postgre_password+"@"+postgre_host+":"+postgre_port+"/"+postgre_db
    SQLALCHEMY_TRACK_MODIFICATIONS = os.getenv("POSTGRE_TRACK_MODIFICATIONS")
