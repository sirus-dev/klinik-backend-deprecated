
import logging.config
import routes
from flask import Flask
from flask_cors import CORS
from flask_compress import Compress
from flask_socketio import SocketIO

from helpers import jwtmanager
from helpers import postgre_alchemy
# from helpers import mongo

cors = CORS()
compress = Compress()
socketio = SocketIO(async_mode='threading', ping_timeout=7200)


def create_app(configuration):
    app = Flask(
        __name__.split(',')[0],
        static_url_path='/static',
        static_folder='../static')

    # register route blueprint
    app.register_blueprint(routes.index.bp)
    app.register_blueprint(routes.error.bp)
    app.register_blueprint(routes.auth.bp)
    app.register_blueprint(routes.role.bp)
    app.register_blueprint(routes.user.bp)
    app.register_blueprint(routes.klinik.bp)

    # load configuration
    app.config.from_object(configuration)

    # init app
    cors.init_app(app, resources={r"/*": {"origins": "*"}})
    compress.init_app(app)
    socketio.init_app(app)
    jwtmanager.init_app(app)
    postgre_alchemy.init_app(app)
    # mongo.init_app(app)

    return app
