import json
from flask import Blueprint
from flask import request
from flask import Response
from helpers.decorator import jwt_check
from helpers.jsonencoder import JSONEncoder
from exceptions import BadRequest
from exceptions import ErrorMessage
from exceptions import SuccessMessage

from controllers import AuthController

bp = Blueprint(__name__, 'user')
authController = AuthController()


@bp.route("/auth/login", methods=["POST"])
# @jwt_check()
def login():

    try:
        # prepare json data
        json_request = request.get_json(silent=True)

        # insert user
        res, user, message = authController.login(json_request)

        # response
        if(res):
            # success response
            response = {"message": "Login successfull",
                        "error": 0, "data": user}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200
        else:
            # success response but no data
            response = {"message": "Login failed, " + message,
                        "error": 1, "data": {}}
            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 200

    except Exception as e:
        # fail response
        response = {"message": "Internal Server Error. " + str(e),
                    "error": 1, "data": {}}
        return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 500
