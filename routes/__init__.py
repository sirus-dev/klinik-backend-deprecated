from . import index
from . import error
from . import user
from . import auth
from . import role
from . import klinik

__all__ = [index, error, user, auth, role, klinik]
