from controllers import UserController
from controllers import RoleController
import requests
import jwt
import json
from flask import current_app, request
from functools import wraps
from helpers import Helper
from exceptions import BadRequest
from exceptions import ErrorMessage
from exceptions import SuccessMessage
from flask import Response
from helpers.jsonencoder import JSONEncoder
from settings import configuration

helper = Helper()
userController = UserController()
roleController = RoleController()


def jwt_check():

    def wrapper(func):

        @wraps(func)
        def decorator(*args, **kwargs):

            token = request.headers.get('Authorization', None)

            if token:
                try:
                    # decode jwt
                    token = token.split(" ")
                    payload = jwt.decode(
                        token[1], configuration.jwt_secret_key, algorithm=[configuration.jwt_algorithm])
                except jwt.DecodeError:
                    # jika token salah (secret key / algoitmanya)
                    response = {"message": "Token Invalid",
                                "error": 1, "data": {}}
                    return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403
                except jwt.ExpiredSignatureError:
                    # jika token sudah expired
                    response = {"message": "Token Expired",
                                "error": 1, "data": {}}
                    return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403
            else:
                # jika ga ada token
                response = {"message": "Token Missing",
                            "error": 1, "data": {}}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

            # pencarian user
            res, user = userController.get_by_id(payload["id"])

            # cek user
            if(("id" in payload) and ("username" in payload) and ("role" in payload) and ("id" in payload["role"])):
                # cek username dan id
                if(user["username"] == payload["username"]):
                    res, roles = roleController.get_by_id(
                        payload["role"]["id"])
                    # jika ada
                    if[res]:
                        can_access = False
                        # looping role yang dia punya
                        for endpoint in roles["endpoint"]:
                            method = request.method
                            path = request.path
                            path = path.split("/")
                            path = "/"+path[1]
                            # cek role
                            if((path == endpoint["path"]) and (method == endpoint["method"])):
                                # jika boleh
                                if(endpoint["access"] == True):
                                    can_access = True
                                else:
                                    # url yg diakses tidak sesuai dengan rolenya
                                    response = {"message": "Access Forbidden",
                                                "error": 1, "data": {}}
                                    return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

                        if(can_access):
                            pass
                        else:
                            # url yg diakses tidak sesuai dengan rolenya
                            response = {"message": "Access Forbidden, url not found",
                                        "error": 1, "data": {}}
                            return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

                else:
                    # username tidak cocok dengan id
                    response = {"message": "User not match",
                                "error": 1, "data": {}}
                    return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403
            else:
                # id tidak ditemukan
                response = {"message": "User not found",
                            "error": 1, "data": {}}
                return Response(json.dumps(response, cls=JSONEncoder), mimetype='application/json'), 403

            return func(*args, **kwargs)

        return decorator

    return wrapper
