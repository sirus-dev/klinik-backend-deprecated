import psycopg2
import psycopg2.extras
from settings import configuration


conn = psycopg2.connect(host=configuration.postgre_host, port=configuration.postgre_port,
                        database=configuration.postgre_db, user=configuration.postgre_user, password=configuration.postgre_password)
cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

postgre_psycopg = cur
