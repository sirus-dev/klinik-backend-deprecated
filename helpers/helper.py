import time
import datetime
import jwt
import json
from passlib.hash import pbkdf2_sha256
from settings import configuration
from flask import Response
from helpers.jsonencoder import JSONEncoder


class Helper(object):

    # def __init__(self, **kwargs):
    #     pass

    def date_to_timestamp(self, date: str):

        timestamp = time.mktime(datetime.datetime.strptime(
            date, "%Y-%m-%dT%H:%M:%S%z").timetuple())

        return int(timestamp)

    @staticmethod
    def generate_hash(password):
        return pbkdf2_sha256.hash(password)

    @staticmethod
    def verify_hash(password, hash):
        return pbkdf2_sha256.verify(password, hash)

    @staticmethod
    def jwt_encode(payload: dict):
        payload["iat"] = datetime.datetime.utcnow()
        payload["exp"] = datetime.datetime.utcnow() + \
            datetime.timedelta(days=1)
        return jwt.encode(payload, configuration.jwt_secret_key, algorithm=configuration.jwt_algorithm).decode("utf-8")
