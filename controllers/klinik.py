from settings.configuration import Configuration
from helpers import Helper
from datetime import datetime
from dateutil import parser
from exceptions import BadRequest
from exceptions import ErrorMessage
from exceptions import SuccessMessage
from models import KlinikModel
from helpers.postgre_alchemy import postgre_alchemy as db
import json


configuration = Configuration()
helper = Helper()

# referensi query model sql alchemy
# https://docs.sqlalchemy.org/en/latest/orm/query.html


class KlinikController(object):

    def __init__(self, **kwargs):
        pass

    def get_all(self, where: dict, sort, limit, skip):

        try:
            # query postgresql
            result = db.session.query(KlinikModel)
            for attr, value in where.items():
                result = result.filter(getattr(KlinikModel, attr) == value)
            result = result.order_by(sort[0]+" "+sort[1])
            result = result.offset(skip).limit(limit)
            result = result.all()

            # change into dict
            klinik = []
            for res in result:
                temp = res.__dict__
                temp.pop('_sa_instance_state', None)
                klinik.append(temp)

            # check if empty
            klinik = list(klinik)
            if (len(klinik) > 0):
                return True, klinik
            else:
                return False, []

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def get_by_id(self, id):

        try:
            # execute database
            result = db.session.query(KlinikModel).get(id)
            if(result):
                result = result.__dict__
                result.pop('_sa_instance_state', None)
            else:
                result = {}

            # change into dict
            klinik = result

            # check if empty
            if klinik:
                return True, klinik
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def create(self, json: dict):

        try:
            # generate json data
            json_send = {}
            json_send = json
            # json_send["created_at"] = datetime.now().isoformat()

            # prepare data model
            result = KlinikModel(**json_send)

            # execute database
            db.session.add(result)
            db.session.commit()
            result = result.to_dict()
            res, klinik = self.get_by_id(result['id'])

            # check if exist
            if(res):
                return True, klinik
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def update(self, where: dict, json: dict):

        try:
            # generate json data
            json_send = {}
            json_send = json
            # json_send["updated_at"] = datetime.now().isoformat()

            try:
                # prepare data model
                result = db.session.query(KlinikModel)
                for attr, value in where.items():
                    result = result.filter(getattr(KlinikModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, klinik = self.get_by_id(where["id"])

                # check if empty
                if (res):
                    return True, klinik
                else:
                    return False, {}

            except Exception as e:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def delete(self, where: dict):

        try:
            # query
            where = where

            try:
                # prepare data model
                result = db.session.query(KlinikModel)
                for attr, value in where.items():
                    result = result.filter(getattr(KlinikModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, klinik = self.get_by_id(where["id"])

                # check if exist
                if(res):
                    return False
                else:
                    return True

            except Exception as e:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})
