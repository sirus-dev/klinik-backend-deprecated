from .user import UserController
from .auth import AuthController
from .role import RoleController
from .klinik import KlinikController

__all__ = ["UserController", "AuthController",
           "RoleController", "KlinikController"]
