import json
from settings.configuration import Configuration
from helpers import Helper
from datetime import datetime
from dateutil import parser
from exceptions import BadRequest
from exceptions import ErrorMessage
from exceptions import SuccessMessage
from helpers.jsonencoder import JSONEncoder
from controllers import UserController
from models import UserModel
from helpers.postgre_psycopg import postgre_psycopg as db

configuration = Configuration()
helper = Helper()
userController = UserController()


class AuthController(object):

    def __init__(self, **kwargs):
        pass

    def login(self, json_data: dict):
        try:
            # query
            json_send = {}
            if("username" not in json_data):
                raise ErrorMessage("Username is missing", 500, 1, {})

            if("password" not in json_data):
                raise ErrorMessage("Password is missing", 500, 1, {})

            # query
            db.execute("select * from users where username='" +
                       json_data['username']+"'")
            res_user = db.fetchall()

            user = []
            for res_user in res_user:
                user.append(dict(res_user))

            # check if empty
            if user:
                user = user[0]
                user_pass_ok = helper.verify_hash(
                    json_data["password"], user["password"])
                if(user_pass_ok):

                    db.execute("select * from role where id='" +
                               str(user['role_id'])+"'")
                    res_role = db.fetchall()

                    role = []
                    for res_role in res_role:
                        role.append(dict(res_role))

                    # check if empty
                    if role:
                        role = role[0]

                        forjwt = {}
                        forjwt["id"] = str(user["id"])
                        forjwt["username"] = user["username"]
                        forjwt2 = {}
                        forjwt2["id"] = str(role["id"])
                        forjwt2["nama"] = str(role["nama"])
                        forjwt["role"] = forjwt2
                        jwt = helper.jwt_encode(forjwt)

                        result = {}
                        result["id"] = str(user["id"])
                        result["username"] = user["username"]
                        result["created_at"] = user["created_at"]
                        result["last_login"] = user["last_login"]
                        result["is_deleted"] = user["is_deleted"]
                        result2 = {}
                        result2["id"] = str(role["id"])
                        result2["nama"] = str(role["nama"])
                        result["role"] = result2
                        result["jwt"] = str(jwt)

                        return True, result, ""

                    else:
                        return False, {}, "Role not found"
                else:
                    return False, {}, "Password not match"
            else:
                return False, {}, "Username not found"

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})
