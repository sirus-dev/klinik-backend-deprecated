from settings.configuration import Configuration
from helpers import Helper
from datetime import datetime
from dateutil import parser
from exceptions import BadRequest
from exceptions import ErrorMessage
from exceptions import SuccessMessage
from models import UserModel
from models import RoleModel
from helpers.postgre_alchemy import postgre_alchemy as db
from sqlalchemy.orm import defer


configuration = Configuration()
helper = Helper()

# referensi query model sql alchemy
# https://docs.sqlalchemy.org/en/latest/orm/query.html


class UserController(object):

    def __init__(self, **kwargs):
        pass

    def get_all(self, where: dict, sort, limit, skip):

        try:
            # query postgresql
            result = db.session.query(UserModel)
            for attr, value in where.items():
                result = result.filter(getattr(UserModel, attr) == value)
            result = result.order_by(sort[0]+" "+sort[1])
            result = result.offset(skip).limit(limit)
            result = result.options(
                defer("password"))
            result = result.all()

            # change into dict
            user = []
            for res in result:
                res = res.__dict__
                res.pop('_sa_instance_state', None)

                # get relation to role
                role = db.session.query(RoleModel)
                role = role.options(
                    defer("created_at"),
                    defer("updated_at"),
                    defer("is_deleted"))
                role = role.get(res['role_id'])

                if(role):
                    role = role.__dict__
                    role.pop('_sa_instance_state', None)
                else:
                    role = {}

                res['role'] = role
                user.append(res)

            # check if empty
            user = list(user)
            if (len(user) > 0):
                return True, user
            else:
                return False, []

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def get_by_id(self, id):

        try:
            # execute database
            result = db.session.query(UserModel)
            result = result.options(
                defer("password"))
            result = result.get(id)

            if(result):
                result = result.__dict__
                result.pop('_sa_instance_state', None)

                # get relation to role
                role = db.session.query(RoleModel)
                role = role.options(
                    defer("created_at"),
                    defer("updated_at"),
                    defer("is_deleted"))
                role = role.get(result['role_id'])

                if(role):
                    role = role.__dict__
                    role.pop('_sa_instance_state', None)
                else:
                    role = {}

                result['role'] = role

            else:
                result = {}

            # change into dict
            user = result

            # check if empty
            if user:
                return True, user
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def get_by_username(self, username):

        try:
            # where
            where = {'username': username}

            # query postgresql
            result = db.session.query(UserModel)
            for attr, value in where.items():
                result = result.filter(getattr(UserModel, attr) == value)
            result = result.one()

            if(result):
                result = result.__dict__
                result.pop('_sa_instance_state', None)
            else:
                result = {}

            # change into dict
            user = result

            # check if empty
            if user:
                return True, user
            else:
                return False, {}

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def create(self, json: dict):

        try:
            # generate json data
            json_send = {}
            json_send = json
            json_send["password"] = helper.generate_hash(json['password'])
            # json_send["created_at"] = datetime.now().isoformat()

            # find if exist
            exist, data = self.get_by_username(json_send["username"])

            # execute database
            if(exist == False):
                # prepare data model
                result = UserModel(**json_send)

                # execute database
                db.session.add(result)
                db.session.commit()
                result = result.to_dict()
                res, user = self.get_by_id(result['id'])

                # check if exist
                if(res):
                    return True, user, ""
                else:
                    return False, {}, ""

            else:
                return False, {}, "username already exist"

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def update(self, where: dict, json: dict):

        try:
            # generate json data
            json_send = {}
            json_send = json
            json_send["password"] = helper.generate_hash(json['password'])
            # json_send["updated_at"] = datetime.now().isoformat()

            try:
                # prepare data model
                result = db.session.query(UserModel)
                for attr, value in where.items():
                    result = result.filter(getattr(UserModel, attr) == value)

                # execute database
                result = result.update(json_send, synchronize_session='fetch')
                result = db.session.commit()
                res, user = self.get_by_id(where["id"])

                # check if empty
                if (res):
                    return True, user
                else:
                    return False, {}

            except Exception as e:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})

    def delete(self, where: dict):

        try:
            # query
            where = where

            try:
                # prepare data model
                result = db.session.query(UserModel)
                for attr, value in where.items():
                    result = result.filter(getattr(UserModel, attr) == value)
                result = result.one()

                # execute database
                db.session.delete(result)
                db.session.commit()
                res, user = self.get_by_id(where["id"])

                # check if exist
                if(res):
                    return False
                else:
                    return True

            except Exception as e:
                # fail response
                raise ErrorMessage("Id not found", 500, 1, {})

        except Exception as e:
            # fail response
            raise ErrorMessage(str(e), 500, 1, {})
