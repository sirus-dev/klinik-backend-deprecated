# Klinik-API (using python)



## About

This is backend service / api for app klinik



## Installation

If you are using **windows**,

First include the python to venv

```
py -3 -m venv venv
```

And activate the venv

```
venv\Scripts\activate
pip install -r requirement.txt
```



If you are using **linux**,

First include the python to venv

```
virtualenv -p python3.5 venv --no-site-packages
```

And activate the venv

```
source venv/bin/activate
pip install -r requirement.txt
```



Open .env file than change the config environment

```
MONGODB_CONN_STRING=
MONGO_DBNAME=
JWT_SECRET_KEY=
PORT=
DEBUG=
```



# Usage

Run the script

```
python run.py
```

You can see which port the api server running
