from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime


class KlinikModel(BaseModel, db.Model):
    """Model for the klinik table"""
    __tablename__ = 'klinik'

    id = db.Column(db.Integer, primary_key=True)
    kode_klinik = db.Column(db.String(255))
    nama_klinik = db.Column(db.String(255))
    alamat_klinik = db.Column(db.String(255))
    no_telp = db.Column(db.String(255))
    tanggal_berdiri = db.Column(db.DateTime(True))
    sk_berdiri = db.Column(db.String(255), nullable=True)
    bank_nama = db.Column(db.String(255), nullable=True)
    bank_cabang = db.Column(db.String(255), nullable=True)
    bank_norek = db.Column(db.String(255), nullable=True)
    no_npwp = db.Column(db.String(255), nullable=True)

    is_deleted = db.Column(db.Boolean, default=False)
    created_at = db.Column(db.DateTime(True), default=db.func.now())
    updated_at = db.Column(db.DateTime(
        True), default=db.func.now(), onupdate=db.func.now())

    def __init__(self, kode_klinik, nama_klinik, alamat_klinik, no_telp,
                 tanggal_berdiri, sk_berdiri, bank_nama, bank_cabang,
                 bank_norek, no_npwp):
        self.kode_klinik = kode_klinik
        self.nama_klinik = nama_klinik
        self.alamat_klinik = alamat_klinik
        self.no_telp = no_telp
        self.tanggal_berdiri = tanggal_berdiri
        self.sk_berdiri = sk_berdiri
        self.bank_nama = bank_nama
        self.bank_cabang = bank_cabang
        self.bank_norek = bank_norek
        self.no_npwp = no_npwp
