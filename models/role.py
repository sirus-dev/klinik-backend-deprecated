from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime


class RoleModel(BaseModel, db.Model):
    """Model for the role table"""
    __tablename__ = 'role'

    id = db.Column(db.Integer, primary_key=True)
    nama = db.Column(db.String(255))
    endpoint = db.Column(db.JSON)

    is_deleted = db.Column(db.Boolean, default=False)
    created_at = db.Column(db.DateTime(True), default=db.func.now())
    updated_at = db.Column(db.DateTime(
        True), default=db.func.now(), onupdate=db.func.now())

    def __init__(self, nama, endpoint):
        self.nama = nama
        self.endpoint = endpoint
