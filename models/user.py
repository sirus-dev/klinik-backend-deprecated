from settings import http, configuration
from models import BaseModel
from helpers.postgre_alchemy import postgre_alchemy as db
import datetime


class UserModel(BaseModel, db.Model):
    """Model for the user table"""
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255))
    password = db.Column(db.String(255))
    role_id = db.Column(db.Integer, db.ForeignKey("role.id"))

    last_login = db.Column(db.DateTime(True), default=db.func.now())
    is_deleted = db.Column(db.Boolean, default=False)
    created_at = db.Column(db.DateTime(True), default=db.func.now())
    updated_at = db.Column(db.DateTime(
        True), default=db.func.now(), onupdate=db.func.now())


def __init__(self, username, password, role_id):
    self.username = username
    self.password = password
    self.role_id = role_id
